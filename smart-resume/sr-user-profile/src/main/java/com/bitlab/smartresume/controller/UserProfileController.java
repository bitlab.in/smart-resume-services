package com.bitlab.smartresume.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserProfileController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
    public String health() {
        return "I'm healthy!";
    }
	

	@RequestMapping(value = "/helloworld", method = RequestMethod.GET)
	@ResponseBody
    public String test() {
        return "Hello World, from SmartResume User Profile Service!";
    }
}

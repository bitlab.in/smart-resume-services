package com.bitlab.smartresume.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ResumeController {

	@GetMapping(value = "/health")
	@ResponseBody
	public String health() {
		return "I'm healthy!";
	}

	@GetMapping(value = "/resumes/{id}")
	@ResponseBody
	public void getResume(@PathVariable Long id) {
		//todo retrieve resume
		//1. query database to get path to S3
		//2. get document from S3
	}
	
	@GetMapping(value = "/resumes/{userId}")
	@ResponseBody
	public void getAllResumeMetadataByUser(@PathVariable Long userId) {
		//todo retrieve metadata for all resumes for a particular userId
	}

	@PostMapping(value = "/resumes")
	@ResponseBody
	public void saveResume(@PathVariable Long userId) {
		//todo save the resume to s3 and store the metadata in mysql
	}
}

### Build the Spring Boot app

```bash
$ mvn package
```

### Build the Spring Boot app and containerize it with Docker

```bash
$ mvn package dockerfile:build
```

Once you've done this, run `docker images` to see the image.


### Deploy (push) container to AWS ECR

Maven has been configured to deploy the container to AWS ECR, provided your machine is set up correctly.

For this to work for you, you need to set up an AWS account, install the AWS cli, create an IAM user with rights to push to ECR, and set up AWS credentials on your machine for it. 

You also need to set an environment variable, `AWS_ACCOUNT_ID`, with the value of your AWS account. 

Next log in to aws ecr:

```bash
$ eval $(aws ecr get-login --no-include-email --region us-east-1)
```

Look for the message "Login Succeeded" and if that doesn't work fix your AWS IAM user / role and proceed.


Next:

```bash
$ mvn deploy
```